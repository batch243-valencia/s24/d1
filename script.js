// console.log("Hello");


// [Section] Exponent operator;

// before ES6

    const firstNum = 8**2;
    console.log(firstNum);

// ES6
    const secondNumber = Math.pow(8,2);
    console.log(secondNumber);
// [Section] Template Literals
    let name= "John";
    // allows to write string w/o using concatenation
    // greatly helps with code readability;
    let message = 'Hello ' + name + '! Welcome to programming!';
    console.log(message);
    // Using template literals
    // using backticks()
    message = `hello ${name}! Welcome to Programming!`;
    console.log(message);


// template literals allow us to write strings w/ embedded javascript expression
const interestRate=0.1;
const principal = 1000;
console.log(`The Interest on your saving is : ${interestRate*principal}.`);

// [Section]Array Destructuring
//  allows us to unpack elements in arrays into distinct variables
//  allows us to name array elements with variables instead of using index numbers

    // Syntax:
    //let/const [varaibleNameA, varaibleNameB,...] =arrayName;
    const fullName= ["Juan", "Dela", "Cruz"];
    // Before array destructuring;
    console.log(fullName[0]);
    console.log(fullName[1]);
    console.log(fullName[2]);
    console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);
    // Array Destructuring
    const [firstName, maidenName,lastName]=fullName;
    console.log(firstName,maidenName,lastName);
    console.log(fullName);
    console.log(`Hello ${firstName} ${maidenName} ${lastName}! It's nice to meet you.`);
//  [Section] Object Destructuring
    // it allows us to unpack properties of object into  distrinct variables
    //      syntax:
    //          let/const {propertyNameA, propertyNameB, propertyNameC,...}=objectName;
    const person ={
        givenName: "Jane",
        maidenName:"Dela",
        familyName:"Cruz"
    }

    // Before the object destructuring

    // DOT notation
    console.log(person.givenName);
    console.log(person.maidenName);
    console.log(person.familyName);
    // Bracket notation
    console.log(person["givenName"]);
    console.log(person["maidenName"]);
    console.log(person["familyName"]);
    // Object Destructuring
    const {givenName,familyName:fName,middleName,}= person;
    console.log (`${givenName} ${maidenName} ${fName}`);

    function getFullName({givenName:fName,maidenName,familyName}){
        console.log(`${fName} ${maidenName} ${familyName}`)
    }
    getFullName(person);
    // [Section] Arrow Functions
    // Compact alternative syntax to traditional functions.
    // usefull for code snippets where creating functions will not be reused in any other portion of the code.


    const hello = ()=>{
        console.log("Hello World");
    }
    hello();
    const hello1 = function(){
        console.log("Hello World");
    }
    hello1();

    // pre-arrow function and template literals
    function printFullNamePreArrow(firstName,middleInitial,lastName){
        console.log(firstName+" "+middleInitial+" " +lastName);
    }
    printFullNamePreArrow("Chris", "O.","Mortel");
    //arrow function and template literals 
    let printfullNameArrow =(firstName,middleInitial,lastName)=>{
        console.log(`${firstName} ${middleInitial} ${lastName}`);
    }
    printfullNameArrow("Chris", "O.","Mortel");

    const students=["John", "Jane", "Judith"];
    // PRE-arrow function
    function iterate(student){
        console.log(student + " is a student!");
    }
    students.forEach(iterate);
    
    // arrow function
    students.forEach((students) =>{
        console.log(`${students} is a student!`)}
        );

    // [Section] Implicit Return Statement
            // there are instances when you can omit return statement
            // this works because even w/o return satement
            // JavaScript implicitly adds if tor the result of the function
            // 
            const add = (x,y) =>{
            // console.log(x+y);
            return x+y;
            };
            let sum = add(23,45);
            console.log("This is the Sum contained in sum variables");
            console.log(sum);
            const substract = (x,y) => x-y;

            substract(10,5);
            let difference = substract (10,5);
            console.log(difference);

            // [Section] Default Function Argument Value
            // provide a default argument value if none is provided when the function is invoked.

            const greet = (name = "Hooman") => {
                return `Good Morning, ${name}`
                ;}

            console.log(greet());

        // [Section] Class-based Object Blueprints
        // allow us to create/instantiation of objects using classes blueprints

        // Creating class
                // constructor is a special method of a class for creating/initializing an object that class
                // syntax:
                // class className{
                //      constructor (objectValueA, ObjectValueB,...)
                //      this.objectPropertyA = objectValueA;
                //      this.objectPropertyB = objectValueB;
                // }

        class Car{
            constructor(brand,name,year){
               this.carBrand = brand; 
               this.carName = name; 
               this.carYear = year; 
            }
        }

        let car= new Car("Toyota","Hilux-pickup",2020);
        console.log(car);

    car.carBrand="Onissan";
    console.log(car);